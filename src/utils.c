#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <setjmp.h>
#include "readcmd.h"
#include "jobs.h"

extern int errno;
extern int pid_fils;
extern int wait_code;
extern int prompting;
extern list jobs;
extern struct cmdline *cmd;
extern jmp_buf goto_prompt;
extern char initcd[];
extern struct sigaction action;

int builtin()
{
    if (cmd == NULL)
    { // EOF
        return 1;
    }
    else if (cmd->err)
    { // error from readcmd
        fprintf(stderr, "ERROR: readcmd failed, %s\n", cmd->err);
        return 2;
    }
    else if (cmd->seq[0] == NULL)
    { // empty
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "exit"))
    { // "exit"
        return 1;
    }
    else if (!strcmp(cmd->seq[0][0], "cd"))
    { // "cd"
        int ret = 0;
        if (cmd->seq[0][1] == NULL)
        { // no path
            ret = chdir(initcd);
        }
        else
        { // with path
            ret = chdir(cmd->seq[0][1]);
        }
        if (ret)
        { // wrong path
            fprintf(stderr, "ERROR: cd failed, (%d) %s\n", errno, strerror(errno));
        }
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "jobs"))
    { // "jobs"
        afficher(&jobs);
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "bg"))
    { // "bg"
        cell *job;
        if (cmd->seq[0][1] == NULL)
        { // no id
            fprintf(stderr, "ERROR: id missing\n");
            return 2;
        }
        else
        { // id specified
            job = trouver_id(&jobs, atoi(cmd->seq[0][1]));
        }
        kill(job->pid, SIGCONT);
        pause();
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "fg"))
    { // "fg"
        cell *job;
        if (cmd->seq[0][1] == NULL)
        { // no id
            fprintf(stderr, "ERROR: id missing\n");
            return 2;
        }
        else
        { // id specified
            job = trouver_id(&jobs, atoi(cmd->seq[0][1]));
            pid_fils = job->pid;
        }
        kill(pid_fils, SIGCONT);
        printf("[%d] %d running: %s\n", job->id, job->pid, job->cmd);
        pause();
        waitpid(pid_fils, NULL, 0);
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "stop"))
    { // "stop"
        cell *job;
        if (cmd->seq[0][1] == NULL)
        { // no id
            fprintf(stderr, "ERROR: id missing\n");
            return 2;
        }
        else
        { // id specified
            job = trouver_id(&jobs, atoi(cmd->seq[0][1]));
        }
        kill(job->pid, SIGSTOP);
        pause();
        return 2;
    }
    else if (!strcmp(cmd->seq[0][0], "pid"))
    { // "pid"
        printf("pid=%d\n", getpid());
        return 2;
    }

    return 0;
}

void handler_sigchld(int sig_num)
{
    do
    {
        pid_fils = waitpid(-1, &wait_code, WNOHANG | WUNTRACED | WCONTINUED);
        if ((pid_fils == -1) && (errno != ECHILD))
        { // wait failed ?
            fprintf(stderr, "ERROR: waiting for %d failed, (%d) %s\n", pid_fils, errno, strerror(errno));
            exit(errno);
        }

        cell *job = trouver(&jobs, pid_fils);
        if (job != NULL)
        {
            if (prompting)
            {
                printf("\n");
            }

            if (WIFSTOPPED(wait_code))
            {
                printf("[%d] %d stopped: %s\n", job->id, job->pid, job->cmd);
            }
            else if (WIFCONTINUED(wait_code))
            {
                printf("[%d] %d continued: %s\n", job->id, job->pid, job->cmd);
                if (!strcmp(cmd->seq[0][0], "fg"))
                {
                    supprimer(&jobs, job->pid);
                }
            }
            else if (WIFEXITED(wait_code))
            {
                printf("[%d] %d exited: %s\n", job->id, job->pid, job->cmd);
                supprimer(&jobs, job->pid);
            }
            else if (wait_code == SIGKILL)
            {
                printf("[%d] %d killed: %s\n", job->id, job->pid, job->cmd);
                supprimer(&jobs, job->pid);
            }
        }
    } while (pid_fils > 0);

    if (prompting)
    {
        siglongjmp(goto_prompt, sig_num);
    }
}

void handler_sigint(int sig_num)
{
    printf("\n");
    if (!prompting)
    {
        kill(pid_fils, SIGKILL);
        pause();
    }
    siglongjmp(goto_prompt, sig_num);
}

void handler_sigtstp(int sig_num)
{
    printf("\n");
    if (!prompting)
    {
        //ajouter(&jobs, pid_fils, *(cmd->seq));
        kill(pid_fils, SIGSTOP);
        pause();
    }
    siglongjmp(goto_prompt, sig_num);
}
