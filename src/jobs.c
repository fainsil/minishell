#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jobs.h"

int ajouter(list *l_ptr, int pid, char **seq)
{
    cell *new_cell = malloc(sizeof(*new_cell));

    char *cmd;
    cmd = malloc(sizeof(char) * 256);
    cmd[0] = '\0';

    while (*seq)
    {
        strcat(cmd, *seq);
        seq++;
        strcat(cmd, " ");
    }

    new_cell->id = 1;
    new_cell->pid = pid;
    new_cell->cmd = cmd;
    new_cell->next = NULL;

    cell *cursor = *l_ptr;
    if (cursor == NULL)
    {
        *l_ptr = new_cell;
    }
    else if (cursor->id > 1)
    {
        new_cell->next = cursor;
        *l_ptr = new_cell;
    }
    else
    {
        while (cursor->next != NULL)
        {
            new_cell->id++;
            if (new_cell->id < cursor->next->id)
            {
                new_cell->next = cursor->next;
                cursor->next = new_cell;
                break;
            }
            cursor = cursor->next;
        }
        if (cursor->next == NULL)
        {
            new_cell->id++;
            cursor->next = new_cell;
        }
    }
    return new_cell->id;
}

void supprimer(list *l_ptr, int pid)
{
    cell *cursor = *l_ptr;

    if (cursor->pid == pid)
    {
        cell *cursor2free = cursor;
        *l_ptr = cursor->next;
        free(cursor2free);
    }
    else
    {
        while (cursor->next != NULL)
        {
            if (cursor->next->pid == pid)
            {
                break;
            }
            else
            {
                cursor = cursor->next;
            }
        }
        cell *cursor_next = cursor->next->next;
        free(cursor->next);
        cursor->next = cursor_next;
    }
}

void afficher(list *l_ptr)
{
    cell *cursor = *l_ptr;
    while (cursor != NULL)
    {
        printf("[%d] %d : %s\n", cursor->id, cursor->pid, cursor->cmd);
        cursor = cursor->next;
    }
}

void initialiser(list *l_ptr)
{
    *l_ptr = NULL;
}

void liberer(list *l_ptr)
{
    cell *cursor2free;
    cell *cursor = *l_ptr;
    while (cursor != NULL)
    {
        cursor2free = cursor;
        cursor = cursor->next;
        free(cursor2free);
    }

    *l_ptr = NULL;
}

cell *trouver(list *l_ptr, int pid)
{
    cell *cursor = *l_ptr;

    while (cursor != NULL)
    {
        if (cursor->pid == pid)
        {
            return cursor;
        }
        cursor = cursor->next;
    }

    return NULL; // erreur
}

cell *trouver_id(list *l_ptr, int id)
{
    cell *cursor = *l_ptr;

    while (cursor != NULL)
    {
        if (cursor->id == id)
        {
            return cursor;
        }
        cursor = cursor->next;
    }

    return NULL; // erreur
}
