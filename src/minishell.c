#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <setjmp.h>
#include "readcmd.h"
#include "jobs.h"
#include "utils.h"

extern int errno;

struct cmdline *cmd;

list jobs;

int pid_fils;
int wait_code;
int builtin_code;

int prompting = 0;
jmp_buf goto_prompt;

char initcd[256], currentcd[256];

int file_in, file_out;

int pipes[256][2];
int sous_fils[256];

struct sigaction action;

int main(int argc, char *argv[])
{
    setvbuf(stdout, NULL, _IONBF, 0); // pratique pour test.sh, optionnel sinon (normalement)
    initialiser(&jobs);
    getcwd(initcd, sizeof(initcd));

    // gestion des signaux
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_SIGINFO | SA_RESTART;
    action.sa_handler = handler_sigchld;
    sigaction(SIGCHLD, &action, NULL);
    action.sa_handler = handler_sigint;
    sigaction(SIGINT, &action, NULL);
    action.sa_handler = handler_sigtstp;
    sigaction(SIGTSTP, &action, NULL);

    // main loop
    while (1)
    {
        sigsetjmp(goto_prompt, 1);

        prompting = 1;
        getcwd(currentcd, sizeof(currentcd));
        printf("%s >>> ", currentcd);
        cmd = readcmd();
        prompting = 0;

        builtin_code = builtin();
        if (builtin_code == 1)
        {
            break;
        }
        else if (builtin_code == 2)
        {
            continue;
        }
        else if (builtin_code != 0)
        {
            fprintf(stderr, "ERROR: builtin command failed");
            exit(1);
        }

        int nb_pipe = -1;
        char ***cursor = cmd->seq;
        while (*cursor)
        { // compter le nombre de commandes séparées par des pipes
            cursor++;
            nb_pipe++;
        }
        if (nb_pipe < 0)
        { // counting failed ?
            fprintf(stderr, "ERROR: counting pipes failed");
            exit(2);
        }

        if ((pid_fils = fork()) == -1)
        { // fork failed ?
            fprintf(stderr, "ERROR: forking failed, (%d) %s\n", errno, strerror(errno));
            exit(errno);
        }
        if (pid_fils == 0)
        { // instructions du fils
            action.sa_handler = SIG_IGN;
            sigaction(SIGTSTP, &action, NULL); // on ignore SIGTSTP
            sigaction(SIGINT, &action, NULL);  // on ignore SIGINT

            if (cmd->in)
            { // "<"
                file_in = open(cmd->in, O_RDONLY);
                dup2(file_in, STDIN_FILENO);
            }
            if (cmd->out)
            { // ">"
                file_out = open(cmd->out, O_CREAT | O_TRUNC | O_WRONLY, 0640);
                dup2(file_out, STDOUT_FILENO);
            }

            if (nb_pipe > 0)
            { // "|"
                for (int i = 0; i <= nb_pipe; i++)
                { // on créé itérativement nb_pipe fils et pipes
                    if (pipe(pipes[i]) < 0)
                    { // pipe failed ?
                        fprintf(stderr, "ERROR: pipe error, (%d) %s\n", errno, strerror(errno));
                        exit(errno);
                    }

                    if ((sous_fils[i] = fork()) < 0)
                    { // fork failed ?
                        fprintf(stderr, "ERROR: fork error, (%d) %s\n", errno, strerror(errno));
                        exit(errno);
                    }
                    else if (sous_fils[i] == 0)
                    { // instructions des sous-fils

                        if (i == 0)
                        { // premier sous-fils
                            close(pipes[0][0]);
                        }
                        else if (dup2(pipes[i - 1][0], STDIN_FILENO) == -1)
                        { // dup2 failed ?
                            fprintf(stderr, "ERROR: dup2 error, (%d) %s\n", errno, strerror(errno));
                            exit(errno);
                        }

                        if (i == nb_pipe)
                        { // dernier sous-fils
                            close(pipes[i][1]);
                        }
                        else if (dup2(pipes[i][1], STDOUT_FILENO) == -1)
                        { // dup2 failed ?
                            fprintf(stderr, "ERROR: dup2 error, (%d) %s\n", errno, strerror(errno));
                            exit(errno);
                        }

                        for (int j = 0; j <= i; j++)
                        { // on ferme les pipes non nécéssaires
                            if (j <= i - 2)
                            { // on ferme tous les pipes que l'on utilise pas
                                close(pipes[j][0]);
                                close(pipes[j][1]);
                            }
                            else if (j == i - 1)
                            { // on ferme l'écriture du pipe précédent
                                close(pipes[j][1]);
                            }
                            else if (j == i)
                            { // on ferme la lecture de son propre pipe
                                close(pipes[j][0]);
                            }
                        }

                        execvp(cmd->seq[i][0], cmd->seq[i]);
                        fprintf(stderr, "ERROR: execvp failed, (%d) %s\n", errno, strerror(errno));
                        exit(errno); // si execvp échoue on exit avec une erreur
                    }
                }
                for (int i = 0; i <= nb_pipe; i++)
                { // on ferme tous les pipes pour le fils
                    close(pipes[i][0]);
                    close(pipes[i][1]);
                }
                for (int i = 0; i <= nb_pipe; i++)
                { // on attend chaque sous-fils
                    if (waitpid(sous_fils[i], &wait_code, 0) == -1)
                        if (wait_code)
                        { // execvp failed ?
                            fprintf(stderr, "ERROR: child n°%d failed, (%d) %s\n", i, wait_code, strerror(wait_code));
                        }
                }
                exit(0); // on termine le fils
            }
            else
            { // pas de pipes dans la commande
                execvp(cmd->seq[0][0], cmd->seq[0]);
                fprintf(stderr, "ERROR: execvp failed, (%d) %s\n", errno, strerror(errno));
                exit(errno); // si execvp échoue on exit avec une erreur
            }
        }
        else
        { // instructions du père
            if (cmd->backgrounded)
            { // background
                int id_fils = ajouter(&jobs, pid_fils, *(cmd->seq));
                printf("[%d] %d\n", id_fils, pid_fils);
            }
            else
            { // foreground
                if (waitpid(pid_fils, &wait_code, 0) == -1)
                { // wait failed ?
                    fprintf(stderr, "ERROR: waiting for %d failed, (%d) %s\n", pid_fils, errno, strerror(errno));
                    exit(errno);
                }
                if (wait_code)
                { // execvp failed ?
                    fprintf(stderr, "ERROR: child failed, (%d) %s\n", wait_code, strerror(wait_code));
                }
            }
        }
    }

    liberer(&jobs);
    return EXIT_SUCCESS;
}
