#ifndef __UTILS_H
#define __UTILS_H

int builtin();

void handler_sigchld(int sig_num);
void handler_sigint(int sig_num);
void handler_sigtstp(int sig_num);

#endif
