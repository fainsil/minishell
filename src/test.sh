green=`tput setaf 2`
reset=`tput sgr0`

extralongpipe=`printf '| sort | uniq | rev %.0s' {1..50}`

rm .testms > /dev/null
touch .testms 
make
clear
tail -f .testms &
tailpid=$!

tmux kill-session -t testms > /dev/null 2>&1
tmux new-session -d -s testms './minishell > .testms'

sleep 0.5

echo "${green}echo bonjour, je suis un message vert de debug${reset}"
tmux send-keys -t testms -l "echo bonjour, je suis un message vert de debug"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}ls${reset}"
tmux send-keys -t testms -l "ls"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}ls -l${reset}"
tmux send-keys -t testms -l "ls -lah"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}ls -lah | grep mini${reset}"
tmux send-keys -t testms -l "ls -lah | grep mini"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}ls -lah | grep mini | wc${reset}"
tmux send-keys -t testms -l "ls -lah | grep mini | wc"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}ls -lah | sort | rev | sort | ... ${reset}"
tmux send-keys -t testms -l "ls -lah $extralongpipe"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}^C${reset}"
tmux send-keys -t testms C-c
echo "${green}^C${reset}"
tmux send-keys -t testms C-c
echo "${green}^C${reset}"
tmux send-keys -t testms C-c

for i in {5..15}
do
    echo "${green}sleep $i &${reset}"
    tmux send-keys -t testms -l "sleep $i &"
    tmux send-keys -t testms Enter
done

sleep 0.5

echo "${green}stop 8${reset}"
tmux send-keys -t testms -l "stop 8"
tmux send-keys -t testms Enter

echo "${green}fg 1${reset}"
tmux send-keys -t testms -l "fg 1"
tmux send-keys -t testms Enter

sleep 0.5

printf "${green}^Z${reset}"
tmux send-keys -t testms C-z

sleep 0.5

echo "${green}bg 1${reset}"
tmux send-keys -t testms -l "bg 1"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}jobs${reset}"
tmux send-keys -t testms -l "jobs"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}pwd${reset}"
tmux send-keys -t testms -l "pwd"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}cd ..${reset}"
tmux send-keys -t testms -l "cd .."
tmux send-keys -t testms Enter
sleep 0.5

echo "${green}pwd${reset}"
tmux send-keys -t testms -l "pwd"
tmux send-keys -t testms Enter

sleep 6

echo "${green}jobs${reset}"
tmux send-keys -t testms -l "jobs"
tmux send-keys -t testms Enter

sleep 7

echo "${green}jobs${reset}"
tmux send-keys -t testms -l "jobs"
tmux send-keys -t testms Enter

sleep 0.5

echo "${green}^D${reset}"
tmux send-keys -t testms C-d
kill $tailpid

printf "\n\n\n"
rm .testms > /dev/null
make clean
