#ifndef __JOBS_H
#define __JOBS_H

typedef struct cell cell;
struct cell
{
  int id;
  int pid;
  char *cmd;
  cell *next;
};

typedef cell *list;

void initialiser(list *list);
void liberer(list *list);
void afficher(cell **list);

int ajouter(cell **list, int pid, char **cmd);
void supprimer(cell **list, int pid);

cell *trouver(list *l_ptr, int pid);
cell *trouver_id(list *l_ptr, int id);

#endif
